FROM anikolaev/cmake:latest

COPY CMakeLists.txt .
COPY src/ ./src
COPY scripts/ ./scripts

WORKDIR ./scripts

CMD ./run.sh
