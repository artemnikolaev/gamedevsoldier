# Gamedev Soldier
## v2.0.2

[![pipeline status](https://gitlab.com/artemnikolaev/gamedevsoldier/badges/master/pipeline.svg)](https://gitlab.com/artemnikolaev/gamedevsoldier/-/commits/master)

[Changelog](CHANGELOG.md)

### Requirements:
To use this repo well, you need some dependencies to be installed:
```$xslt
- Linux(optional)
- CMAke
- g++(or other cpp compiler)
- SDL2
```

### Local testing:
Generating and building apps:
```bash
# Step 1: Project File Generation
cmake -S . -B ./build

# Step 2: Build
cmake --build ./build
```
After that you can run builded apps in ./build folder.

Also you can do it through the bash:
```bash
# Step 1-2 Alternative 1: using Bash
./scripts/_generate.sh #for generate step
./scripts/_build.sh #for build step
./scripts/_test.sh #for testing builded apps
```

```bash
# Step 1-2 Alternative 2: single Bash script for all steps
./scripts/run.sh #for build step
```

### Test in Docker
Also you can generate, build and test apps inside Docker container:
```bash
# Step 1: create docker image
docker build -t game-dev-soldier .

# Step 2: run docker image
docker run game-dev-soldier
```