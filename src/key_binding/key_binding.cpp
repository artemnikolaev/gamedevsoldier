#include <SDL2/SDL.h>

#include <algorithm>
#include <array>
#include <cstdlib>
#include <iostream>
#include <string_view>

std::ostream& operator<<(std::ostream& out, const SDL_version& v) {
  out << static_cast<int>(v.major) << '.';
  out << static_cast<int>(v.minor) << '.';
  out << static_cast<int>(v.patch);
  return out;
}

#pragma pack(push, 4)
struct bind {
  SDL_Keycode key;
  std::string_view name;
};
#pragma pack(pop)

void check_cinput(const SDL_Event& e) {
  using namespace std;

  const array<::bind, 17> ckeys{
      {{SDL_CONTROLLER_BUTTON_INVALID, "invalid"},
       {SDL_CONTROLLER_BUTTON_A, "cross"},
       {SDL_CONTROLLER_BUTTON_B, "circle"},
       {SDL_CONTROLLER_BUTTON_X, "square"},
       {SDL_CONTROLLER_BUTTON_Y, "triange"},
       {SDL_CONTROLLER_BUTTON_BACK, "back"},
       {SDL_CONTROLLER_BUTTON_GUIDE, "guide"},
       {SDL_CONTROLLER_BUTTON_START, "start"},
       {SDL_CONTROLLER_BUTTON_LEFTSTICK, "left_stick"},
       {SDL_CONTROLLER_BUTTON_RIGHTSTICK, "right_stick"},
       {SDL_CONTROLLER_BUTTON_LEFTSHOULDER, "left_shoulder"},
       {SDL_CONTROLLER_BUTTON_RIGHTSHOULDER, "right_shoulder"},
       {SDL_CONTROLLER_BUTTON_DPAD_UP, "dpad_up"},
       {SDL_CONTROLLER_BUTTON_DPAD_DOWN, "dpad_down"},
       {SDL_CONTROLLER_BUTTON_DPAD_LEFT, "dpad_left"},
       {SDL_CONTROLLER_BUTTON_DPAD_RIGHT, "dpad_right"},
       {SDL_CONTROLLER_BUTTON_MAX, "max"}}};

  const auto cit = find_if(begin(ckeys), end(ckeys), [&](const ::bind& b) {
    return b.key == e.cbutton.button;
  });

  if (cit != end(ckeys)) {
    cout << cit->name << ' ';
    if (e.type == SDL_CONTROLLERBUTTONDOWN) {
      cout << "is pressed" << endl;
    } else {
      cout << "is released" << endl;
    }
  }
}

void check_caxis(const SDL_Event& e) {
  using namespace std;

  if (e.caxis.value <= 4000 && e.caxis.value >= -4000) {
    return;
  }

  const array<::bind, 17> ckeys{
      {{SDL_CONTROLLER_AXIS_INVALID, "invalid"},
       {SDL_CONTROLLER_AXIS_LEFTX, "left_x"},
       {SDL_CONTROLLER_AXIS_LEFTY, "left_y"},
       {SDL_CONTROLLER_AXIS_RIGHTX, "right_x"},
       {SDL_CONTROLLER_AXIS_RIGHTY, "right_y"},
       {SDL_CONTROLLER_AXIS_TRIGGERLEFT, "trigger_left"},
       {SDL_CONTROLLER_AXIS_TRIGGERRIGHT, "trigger_right"},
       {SDL_CONTROLLER_AXIS_MAX, "max"}}};

  const auto cit = find_if(begin(ckeys), end(ckeys), [&](const ::bind& b) {
    return b.key == e.cbutton.button;
  });

  if (cit != end(ckeys)) {
    if (cit->name == "left_x" || cit->name == "left_y" ||
        cit->name == "right_x" || cit->name == "right_y") {
      return;
    }

    cout << cit->name << ' ' << e.caxis.value << endl;
  }
}

void check_jaxis(const SDL_Event& e) {
  printf("Axis: %i\nValue: %hd\n\n", e.jaxis.axis, e.jaxis.value);
}

void check_input(const SDL_Event& e) {
  using namespace std;

  const array<::bind, 8> keys{{{SDLK_w, "up"},
                               {SDLK_a, "left"},
                               {SDLK_s, "down"},
                               {SDLK_d, "right"},
                               {SDLK_LCTRL, "button_one"},
                               {SDLK_SPACE, "button_two"},
                               {SDLK_ESCAPE, "select"},
                               {SDLK_RETURN, "start"}}};

  const auto it = find_if(begin(keys), end(keys), [&](const ::bind& b) {
    return b.key == e.key.keysym.sym;
  });

  if (it != end(keys)) {
    cout << it->name << ' ';
    if (e.type == SDL_KEYDOWN) {
      cout << "is pressed" << endl;
    } else {
      cout << "is released" << endl;
    }
  }
}

int main(int /*argc*/, char* /*argv*/[]) {
  using namespace std;

  SDL_version compiled = {0, 0, 0};
  SDL_version linked = {0, 0, 0};

  SDL_VERSION(&compiled)
  SDL_GetVersion(&linked);

  if (SDL_COMPILEDVERSION !=
      SDL_VERSIONNUM(linked.major, linked.minor, linked.patch)) {
    cerr << "warning: SDL2 compiled and linked version mismatch: " << compiled
         << " " << linked << endl;
  }

  const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
  if (init_result != 0) {
    const char* err_message = SDL_GetError();
    cerr << "error: failed call SDL_Init: " << err_message << endl;
    return EXIT_FAILURE;
  }

  SDL_GameControllerAddMappingsFromFile("../gamecontrollerdb.txt");

  SDL_GameController* ctrl;
  int i;
  for (i = 0; i < SDL_NumJoysticks(); ++i) {
    if (SDL_IsGameController(i)) {
      char* mapping;
      SDL_Log("Index '%i' is a compatible controller, named '%s'", i,
              SDL_GameControllerNameForIndex(i));
      ctrl = SDL_GameControllerOpen(i);
      mapping = SDL_GameControllerMapping(ctrl);
      SDL_Log("Controller %i is mapped as '%s'.", i, mapping);
      SDL_free(mapping);
    } else {
      SDL_Log("Index '%i' is not a compatible controller.", i);
      SDL_Log("Device name: %s", SDL_JoystickNameForIndex(i));
      SDL_Joystick* joy = SDL_JoystickOpen(i);
      SDL_Log("Device type: %u", SDL_JoystickGetType(joy));
      SDL_Log("Device axes amount: %i", SDL_JoystickNumAxes(joy));
      SDL_Log("Device balls amount: %i", SDL_JoystickNumBalls(joy));
      SDL_Log("Device hats amount: %i", SDL_JoystickNumHats(joy));
      SDL_Log("Device buttons amount: %i", SDL_JoystickNumButtons(joy));
    }
  }

  SDL_Window* const window =
      SDL_CreateWindow("title", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                       640, 480, ::SDL_WINDOW_OPENGL);

  if (window == nullptr) {
    const char* err_message = SDL_GetError();
    cerr << "error: failed call SDL_CreateWindow: " << err_message << endl;
    SDL_Quit();
    return EXIT_FAILURE;
  }

  bool continue_loop = true;
  while (continue_loop) {
    SDL_Event sdl_event;

    while (SDL_PollEvent(&sdl_event)) {
      switch (sdl_event.type) {
        case SDL_KEYDOWN:
        case SDL_KEYUP:
          check_input(sdl_event);
          break;
        case SDL_JOYBUTTONDOWN:
        case SDL_JOYBUTTONUP:
        case SDL_CONTROLLERBUTTONDOWN:
        case SDL_CONTROLLERBUTTONUP:
          check_cinput(sdl_event);
          break;
        case SDL_CONTROLLERAXISMOTION:
          check_caxis(sdl_event);
          break;
        case SDL_JOYAXISMOTION:
          check_jaxis(sdl_event);
          break;
        case SDL_QUIT:
          /* Attempt to open every controller. */
          for (int i = 0; i < SDL_NumJoysticks(); ++i) {
            if (SDL_IsGameController(i)) {
              SDL_GameController* controller = SDL_GameControllerOpen(i);
              if (controller) {
                SDL_GameControllerClose(controller);
              } else {
                fprintf(stderr, "Could not open gamecontroller %i: %s\n", i,
                        SDL_GetError());
              }
            }
          }
          continue_loop = false;
          break;
        default:
          break;
      }
    }
  }

  SDL_DestroyWindow(window);

  SDL_Quit();

  return EXIT_SUCCESS;
}
