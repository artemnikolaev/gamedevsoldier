project(sdl_version_shared)

SET(CMAKE_CXX_FLAGS -pthread)

if (APPLE)
    include_directories(/usr/local/include/)
    find_library(SDL2_LIB libSDL2.dylib)
elseif (LINUX)
    find_library(SDL2_LIB libSDL2.so)
endif ()

add_executable(
        key_binding_app
        src/key_binding/key_binding.cpp
)

target_link_libraries(
        key_binding_app
        PRIVATE
        ${CMAKE_DL_LIBS} ${SDL2_LIB}
)
