project(game-engine)

SET(CMAKE_CXX_FLAGS -pthread)

if (APPLE)
    include_directories(/usr/local/include/)
    find_library(SDL2_LIB libSDL2.dylib)
elseif (LINUX)
    find_library(SDL2_LIB libSDL2.so)
endif ()

# lib for checking sdl library version
add_library(_check_sdl_version STATIC src/game/engine/check_sdl_version/check_sdl_version.h src/game/engine/check_sdl_version/check_sdl_version.cpp)
target_link_libraries(_check_sdl_version PRIVATE ${CMAKE_DL_LIBS} ${SDL2_LIB})

# main engine lib
add_library(_engine SHARED src/game/engine/engine.h src/game/engine/engine.cpp)
target_link_libraries(_engine PRIVATE ${CMAKE_DL_LIBS} ${SDL2_LIB} _check_sdl_version)

add_executable(game src/game/game.cpp)
target_link_libraries(
        game
        PRIVATE _engine
)
