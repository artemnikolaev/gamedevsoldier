project(sdl_version_shared)

SET(CMAKE_CXX_FLAGS -pthread)

if (APPLE)
    find_library(SDL2_LIB libSDL2.dylib)
elseif (LINUX)
    find_library(SDL2_LIB libSDL2.so)
endif ()

add_executable(
        sdl_version_shared_app
        src/sdl_version/sdl_version.cpp
)

target_link_libraries(
        sdl_version_shared_app
        PRIVATE
        ${CMAKE_DL_LIBS} ${SDL2_LIB}
)
