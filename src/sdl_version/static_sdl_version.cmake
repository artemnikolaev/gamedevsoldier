project(sdl_version_static)

SET(CMAKE_CXX_FLAGS -pthread)

find_library(SDL2_LIB libSDL2.a)

add_executable(
        sdl_version_static_app
        src/sdl_version/sdl_version.cpp
)

target_link_libraries(
        sdl_version_static_app
        PRIVATE
        ${CMAKE_DL_LIBS} ${SDL2_LIB}
)
