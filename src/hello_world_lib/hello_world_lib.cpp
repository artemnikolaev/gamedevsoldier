#include "hello_world_lib.h"

#include <iostream>

namespace hello {
int world::run() {
  using namespace std;

  cout << "Hello, World!" << endl;

  return cout.fail();
}
}  // namespace hello