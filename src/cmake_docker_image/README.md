# Docker Image for gamedev courses
### name: `anikolaev/cmake`
### link: [DockerHub](https://hub.docker.com/repository/docker/anikolaev/cmake/general)
### v0.3.0

## Content:
- Fedora 33
- Make
- CMake
- Ninja-Build
- G++
- SDL2 (both shared and static)

## How to upload

```bash
# login to dockerhub
docker login --username=yourhubusername

# build image, be sure you are in current folder
docker build . -t anikolaev/cmake:latest -t anikolaev/cmake:version

# push it to dockerhub
docker push anikolaev/cmake:version

# and of course push latest tag too
docker push anikolaev/cmake:latest
```

#Changelog
## [0.3.0] - 2020-04-03
### Added
- SDL2 shared and static library

## [0.2.0] - 2020-04-01
### Changed
- content for container was moved to special folder
- readme content
- changelog stuff

## [0.1.0] - 2020-03-23
### Added
- dockerfile
- fedora 33
- make
- cmake
- ninja-build
- g++
