# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [3.0.0] - 2020-04-10
### Changed
- Project to run well on mac
- bindings project
- controller button's bindings
- controller axis bindings
- working dll engine version
- steering wheel connection

## [2.0.2] - 2020-04-03
### Added
- Add SDL2 to system requirements

## [2.0.1] - 2020-04-03
### Added
- Pipeline badge right in the README.md file

## [2.0.0] - 2020-04-03
### Added
- Project for sdl_version, static and shared
- add sdl_version project to `CI/CD` and to local scripts
- SDL2 shared and static libraries for cmake docker image
- Content description in docker image readme
- add `project()` command to `hello_world` and `hello_world_lib` cmake files
### Changed
- builds from default to `Debug` configuration

## [1.4.0] - 2020-04-02
### Fixed
- hello_world with shared library. Previously it works wrong because of syntax error

## [1.3.1] - 2020-04-01
### Changed
- cmake image moved in special folder
- root Docker file use cmake image and have only test commands inside
- scripts file structure. Instead of `_` in name files moved to `sub` direction

## [1.3.0] - 2020-04-01
### Changed
- dockerignore rules. There's a problem with `.git` or `cmake-*` patterns, so i choose to run it from exclude + exception from exclude
- file structure, project files moves to `src` folder

## [1.2.2] - 2020-03-31
### Added
- clang-format file

### Changed
- Existed code reformated with clang-format

## [1.2.1] - 2020-03-31
### Changed
- Rewrite cmakelists to only run included files, so now we have project-specific cmake files in project folder 

## [1.2.0] - 2020-03-30
### Changed
- names for artifacts archives
- artifacts for generated step creates only if job failed
- artifacts for build step creates only if job success

## [1.1.0] - 2020-03-28
### Changed
- bash scripts behavior
- Dockerfile behavior

## [1.0.0] - 2020-03-28
### Added
- hello_world app
- hello_world_lib app
- CMake configs for hello_world_lib app
### Changed
- ci/cd jobs manner
- run.sh script

## [0.1.0] - 2020-03-23
### Added
- readme file
- changelog info
- hello world app
- gitignore file
- script for generate and build app
- dockerfile for containerization
- gitlab ci/cd implementation